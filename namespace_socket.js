var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const redis = require("redis");
let redis_port = 6379;
const client = redis.createClient(redis_port);
 
client.on("error", function(error) {
  console.error(error);
});
client.on("connect", function() {
  console.log("Redis Connected!!");
});

app.get('/', function(req, res) {
   res.sendfile('namespace_socket.html');
});

app.get('/editor', (req, res)=>{
    res.sendfile('editor_space.html')
})


let name_spaces = ["namespace", "namespace1"]

var nsp = io.of(`/${name_spaces[0]}`);
nsp.on('connection', function(socket) {
    console.log("someone connected!")
    client.get(name_spaces[0], (err, data)=>{
      if (err) throw err;
      if (data != null){
          nsp.emit('text3', JSON.parse(data))
      }else{
          return
      }
    });
    socket.on('text2', function(data) {
      client.setex(name_spaces[0],360000,JSON.stringify(data))
      nsp.emit('text3', data);
    });
  });

http.listen(3000, function() {
   console.log('listening on localhost:3000');
});