var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
   res.sendfile('index.html');
});

app.get('/index1', function(req, res) {
    res.sendfile('index1.html');
 });

 app.get('/editor', function(req, res) {
    res.sendfile('editor.html');
 });

let nsp = io.of('/my-namespace')
let socket_data = ''
let clients = 0
let roomno = 1;

nsp.on('connection', function(socket) {
    console.log('someone connected');
    nsp.emit('hi', 'Hello everyone!');
 });

io.on('connection', function(socket) {
    clients++;
    io.sockets.emit('broadcast', { description: clients + 'clients connected!' })
    console.log(`${clients} user connected`);
    socket.on('clientEvent', function(data) {
        socket_data = data
     });
 
    setTimeout(function() {
        socket.send('Sent a message 4 second after connection!')
    }, 4000);

    setTimeout(function(){
        socket.emit('testerEvent',{description: socket_data})
    }, 4000);

    socket.on('text2', function(data1){
      io.sockets.emit('updatedata1', {yes: data1})
   })

   //socket.emit("yes", "hhhhhhh"})

    socket.on('disconnect', function () {
        clients--;
       console.log(`1 user disconnected and remain ${clients}`);
    });

    if(io.nsps['/'].adapter.rooms["room-"+roomno] && io.nsps['/'].adapter.rooms["room-"+roomno].length > 1) roomno++;
    socket.join("room-"+roomno);
 
    //Send this event to everyone in the room.
    io.sockets.in("room-"+roomno).emit('connectToRoom', "You are in room no. "+roomno);
 
 });

http.listen(3000, function() {
   console.log('listening on *:3000');
});