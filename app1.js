var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
   res.sendfile('index2.html');
});

users = [];
let clients = 0
io.on('connection', function(socket) {
   clients++;
    io.sockets.emit('active_user', { description: clients + ' : active users!' })
    console.log(`${clients} user connected`);
   socket.on('setUsername', function(data) {
      console.log(data);
      
      if(users.indexOf(data) > -1) {
         socket.emit('userExists', data + ' username is taken! Try some other username.');
      } else {
         users.push(data);
         socket.emit('userSet', {username: data});
      }
   });

   
   socket.on('msg', function(data) {
      //Send message to everyone
      io.sockets.emit('newmsg', data);
   })
   socket.on('disconnect', function () {
    clients--;
   console.log(`1 user disconnected and remain ${clients}`);
});
});

http.listen(3000, function() {
   console.log('listening on localhost:3000');
});